﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace backyarDLib
{
    static class CalcLib
    {
        /// <summary>
        /// 順列組み合わせを洗い出す。配列を作り、hairetsu.Permutation() で結果が戻る。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<T>> Permutation<T>(this IEnumerable<T> items, int? k = null)
        {
            if (k == null)
                k = items.Count();

            if (k == 0)
            {
                yield return Enumerable.Empty<T>();
            }
            else
            {
                var idx = 0;
                foreach (var x in items)
                {
                    var xs = items.Where((_, index) => idx != index);
                    foreach (var c in Permutation(xs, k - 1))
                        yield return c.Before(x);

                    idx++;
                }
            }
        }

        /// <summary>
        /// 要素をシーケンスに追加。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="first"></param>
        /// <returns></returns>
        public static IEnumerable<T> Before<T>(this IEnumerable<T> items, T first)
        {
            yield return first;

            foreach (var i in items)
                yield return i;
        }

        /// <summary>
        /// 組み合わせを洗い出す。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<T>> Combination<T>(this IEnumerable<T> items, int r)
        {
            if (r == 0)
            {
                yield return Enumerable.Empty<T>();
            }
            else
            {
                var idx = 1;
                foreach (var x in items)
                {
                    var xs = items.Skip(idx);
                    foreach (var c in Combination(xs, r - 1))
                        yield return c.Before(x);

                    idx++;
                }
            }
        }
    }
}
