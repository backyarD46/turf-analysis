﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace backyarDLib
{
    public partial class InputForm : Form
    {
        /// <summary>
        /// コンストラクター。
        /// </summary>
        public InputForm()
        {
            InitializeComponent();
        }

        private void WriteLog(string message)
        {
            logList.Items.Add(DateTime.Now.ToString() + "\t" + message);
            logList.TopIndex = logList.Items.Count - 1;
        }

        /// <summary>
        /// 列追加ボタン。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addColButton_Click(object sender, EventArgs e)
        {
            DataGridViewColumn newCol = new DataGridViewCheckBoxColumn();
            newCol.DataPropertyName = "New Col";
            newCol.Name = "Col";
            newCol.HeaderText = "Col " + (mainGrid.Columns.Count - 1 < 0 ? 0 : mainGrid.Columns.Count).ToString();
            mainGrid.Columns.Add(newCol);
        }

        /// <summary>
        /// 行追加ボタン。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addRowButton_Click(object sender, EventArgs e)
        {
            DataGridViewRow newRow = new DataGridViewRow();
            mainGrid.Rows.Add(newRow);
        }

        /// <summary>
        /// 閉じるボタン。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 分析実行ボタン。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void analyzeButton_Click(object sender, EventArgs e)
        {
            WriteLog("％最大の列組み合わせ: [" + string.Join(",", Analyze(2)) + "]");
        }

        /// <summary>
        /// グリッドの内容をBitArrayのListに変換する。
        /// </summary>
        /// <returns>チェックありをTrue、なしをFalseにしたBitArrayのList。</returns>
        private List<BitArray> ScanGrid()
        {
            WriteLog("一覧表スキャン開始。");
            List<BitArray> result = new List<BitArray>();

            // 列をスキャン
            for (int idx = 0; idx < mainGrid.Columns.Count; idx++)
            {
                // 行をスキャン
                BitArray colData = new BitArray(mainGrid.Rows.Count);
                for (int row = 0; row < mainGrid.Rows.Count; row++)
                {
                    colData[row] = (bool)mainGrid[idx, row].FormattedValue;
                }
                result.Add(colData);
            }
            return result;
        }

        /// <summary>
        /// BitArray1件の中でTrueが含まれる割合を取得する。
        /// </summary>
        /// <param name="target">対象BitArray。</param>
        /// <returns>結果。</returns>
        private float CalcShare(BitArray target)
        {
            // ログメッセージ処理
            bool[] bitSeq = new bool[target.Count];
            target.CopyTo(bitSeq, 0);
            string message = "％計算:" + string.Join(",", bitSeq) + " => ";
            // 計算
            int total = target.Length;
            int values = 0;
            foreach (bool bit in target)
            {
                if (bit) values++;
            }
            float result = (float)100 * values / total;

            WriteLog(message + result.ToString());
            return result;
        }

        /// <summary>
        /// 列数に対応した1～列数の整数配列を作る。
        /// </summary>
        /// <returns></returns>
        private int[] GetColNoArray()
        {
            int[] result = new int[mainGrid.Columns.Count];
            for (int idx = 0; idx < mainGrid.Columns.Count; idx++)
            {
                result[idx] = idx;
            }
            return result;
        }

        /// <summary>
        /// 分析実行。
        /// </summary>
        /// <param name="selectionCount">選択する列の数。</param>
        /// <returns>割合が最大値になる列の組み合わせを表す整数配列。</returns>
        private int[] Analyze(int selectionCount)
        {

            // 一覧表をBitArrayに書き写す
            List<BitArray> dataMatrix = ScanGrid();
            // 列の組み合わせを生成する
            var combinations = GetColNoArray().Combination(2).ToArray();
            var message = new StringBuilder();
            foreach (var combination in combinations)
            {
                message.Append("[" + string.Join(",", combination.ToArray()) + "] ");
            }
            WriteLog("列組み合わせ洗い出し: " + message.ToString());
            // 組み合わせ数に対応した結果を入れる配列
            float[] percentages = new float[combinations.Count()];
            // 列の組み合わせそれぞれに対してパーセンテージを出す
            for (int idx = 0; idx < combinations.Count(); idx++)
            {
                int[] target = combinations[idx].ToArray();
                WriteLog("組み合わせ [" + string.Join(", ", target) + "] を検証");
                BitArray calcTarget = new BitArray(mainGrid.Rows.Count);
                foreach (int colNo in target)
                {
                    calcTarget = calcTarget.Or(dataMatrix[colNo]);
                }
                percentages[idx] = CalcShare(calcTarget);
            }
            // パーセンテージが最大の組み合わせ番号をFindIndexで探し、結果として返す
            return combinations[Array.FindIndex(percentages, x => x == percentages.Max())].ToArray();
        }
    }
}
